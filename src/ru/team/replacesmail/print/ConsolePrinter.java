package ru.team.replacesmail.print;

import ru.team.replacesmail.print.printer.Printer;

public class ConsolePrinter implements Printer {
    @Override
    public void print(String text) {
        System.out.println(text);
    }
}
