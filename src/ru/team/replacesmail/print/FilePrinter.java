package ru.team.replacesmail.print;

import ru.team.replacesmail.print.printer.Printer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FilePrinter implements Printer {

    private final String path;

    public FilePrinter(final String path) {
        this.path = path;
    }

    @Override
    public void print(final String text) {
        try {
            Files.write(Paths.get(path), text.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
