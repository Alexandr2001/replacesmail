package ru.team.replacesmail.print.printer;

public interface Printer {
    void print(String text);
}
