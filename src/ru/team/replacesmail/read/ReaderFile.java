package ru.team.replacesmail.read;

import ru.team.replacesmail.read.reader.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReaderFile implements Reader {
    private String path;

    public ReaderFile(String path){
        this.path = path;
    }

    @Override
    public String read() {
        StringBuilder str = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            String text;
            while ((text = bufferedReader.readLine()) != null) {
                str.append(text).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return String.valueOf(str);
    }
}
