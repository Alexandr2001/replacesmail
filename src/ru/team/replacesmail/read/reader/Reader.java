package ru.team.replacesmail.read.reader;

public interface Reader {
    String read();
}
