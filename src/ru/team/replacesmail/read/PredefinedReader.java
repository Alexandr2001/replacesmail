package ru.team.replacesmail.read;

import ru.team.replacesmail.read.reader.Reader;

public class PredefinedReader implements Reader {
    private String string;

    public PredefinedReader(String string) {
        this.string = string;
    }

    @Override
    public String read() {
        return string;
    }
}
