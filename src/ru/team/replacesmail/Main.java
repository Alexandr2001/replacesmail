package ru.team.replacesmail;

import ru.team.replacesmail.print.ConsolePrinter;
import ru.team.replacesmail.print.FilePrinter;
import ru.team.replacesmail.print.printer.Printer;
import ru.team.replacesmail.processing.Replacer;
import ru.team.replacesmail.read.PredefinedReader;
import ru.team.replacesmail.read.ReaderFile;
import ru.team.replacesmail.read.reader.Reader;

public class Main {
    public static void main(String[] args) {
        Reader reader = new PredefinedReader("Какая-то строка со смайлом =( ");
        Printer printer = new ConsolePrinter();
        Replacer replacer = new Replacer(reader, printer);
        replacer.replace();

        Reader readerFile = new ReaderFile("src\\ru\\team\\replacesmail\\file.txt");
        Printer printerFile = new FilePrinter("src\\ru\\team\\replacesmail\\result.txt");
        Replacer replacerFile = new Replacer(readerFile,printerFile);
        replacerFile.replace();
    }
}
