package ru.team.replacesmail.processing;

import ru.team.replacesmail.print.printer.Printer;
import ru.team.replacesmail.read.reader.Reader;

public class Replacer {

    private Reader reader;
    private Printer printer;

    public Replacer(Reader reader, Printer printer) {
        this.reader = reader;
        this.printer = printer;
    }

    public void replace() {
        printer.print(reader.read().replaceAll("=\\(", "=)"));
    }
}
